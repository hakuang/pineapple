<?php

//BOOTSTRAP

define('BOOTSTRAP_VERSION', 3);


//WEBSITE

define('WEBSITE_NAME', "PineApple");

define('WEBSITE_DOMAIN', "http://pineappletele.com");

//it can be the same as domain (if script is placed on website's root folder) 
//or it can cotain path that include subfolders, if script is located in some subfolder and not in root folder
define('SCRIPT_URL', "http://pineappletele.com/signin/");


//DATABASE CONFIGURATION

define('DB_HOST', "localhost"); 

define('DB_TYPE', "mysql"); 

define('DB_USER', "maxkuang1994"); 

define('DB_PASS', "Pineapple110"); 

define('DB_NAME', "PineAppleDB"); 


//SESSION CONFIGURATION

define('SESSION_SECURE', false);   

define('SESSION_HTTP_ONLY', true);

define('SESSION_REGENERATE_ID', true);   

define('SESSION_USE_ONLY_COOKIES', 1);


//LOGIN CONFIGURATION

define('LOGIN_MAX_LOGIN_ATTEMPTS', 15); 

define('LOGIN_FINGERPRINT', false); 

define('SUCCESS_LOGIN_REDIRECT', serialize(array( 'default' => "index.php"))); 


//PASSWORD CONFIGURATION

define('PASSWORD_ENCRYPTION', "bcrypt"); //available values: "sha512", "bcrypt"

define('PASSWORD_BCRYPT_COST', "13"); 

define('PASSWORD_SHA512_ITERATIONS', 25000); 

define('PASSWORD_SALT', "l2mDupQe4iGQMlrjjH88ro"); //22 characters to be appended on first 7 characters that will be generated using PASSWORD_ info above

define('PASSWORD_RESET_KEY_LIFE', 60); 


//REGISTRATION CONFIGURATION

define('MAIL_CONFIRMATION_REQUIRED', false); 

define('REGISTER_CONFIRM', "http://pineappletele.com/signin/confirm.php"); 

define('REGISTER_PASSWORD_RESET', "http://pineappletele.com/signin/passwordreset.php"); 


//EMAIL SENDING CONFIGURATION

define('MAILER', "mail"); // available options are 'mail' for php mail() and 'smtp' for using SMTP server for sending emails

define('SMTP_HOST', ""); 

define('SMTP_PORT', 0); 

define('SMTP_USERNAME', ""); 

define('SMTP_PASSWORD', ""); 

define('SMTP_ENCRYPTION', ""); 


//SOCIAL LOGIN CONFIGURATION

define('SOCIAL_CALLBACK_URI', "http://pineappletele.com/signin/vendor/hybridauth/"); 


// GOOGLE

define('GOOGLE_ENABLED', false); 

define('GOOGLE_ID', ""); 

define('GOOGLE_SECRET', ""); 


// FACEBOOK

define('FACEBOOK_ENABLED', false); 

define('FACEBOOK_ID', ""); 

define('FACEBOOK_SECRET', ""); 


// TWITTER

// NOTE: Twitter api for authentication doesn't provide users email address!
// So, if you email address is strictly required for all users, consider disabling twitter login option.

define('TWITTER_ENABLED', false); 

define('TWITTER_KEY', ""); 

define('TWITTER_SECRET', ""); 


// TRANSLATION

define('DEFAULT_LANGUAGE', 'en'); 



<?php

include dirname(__FILE__) . '/signin/ASEngine/ASUser.php';
include dirname(__FILE__) . '/signin/ASEngine/AS.php';

$userName = 'Account';
if( $login->isLoggedIn() ){
$user = new ASUser(ASSession::get("user_id"));
$userInfo = $user->getInfo();
$userName = e($userInfo['username']);  
$userDetails = $user->getDetails();
}
else
$userName = 'Account';
?>
<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
<meta content="IE=edge" http-equiv="X-UA-Compatible">
<meta content="width=device-width, initial-scale=1" name="viewport">
<meta content="" name="description">
<meta content="" name="author">
<title>PineApple</title>
<!-- Bootstrap Core CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet">
<!-- Custom CSS -->
<link href="css/agency.css" rel="stylesheet">
<link href="css/PineApp.css" rel="stylesheet">
<!-- Custom Fonts -->
<link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
<link href="http://fonts.googleapis.com/css?family=Kaushan+Script" rel="stylesheet" type="text/css">
<link href="http://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic" rel="stylesheet" type="text/css">
<link href="http://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700" rel="stylesheet" type="text/css">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// --><!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body id="page-top" class="index">

<!-- Navigation -->
<nav class="navbar navbar-default navbar-fixed-top">
	<div class="container">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header page-scroll">
			<button class="navbar-toggle" data-target="#bs-example-navbar-collapse-1" data-toggle="collapse" type="button">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span><span class="icon-bar"></span>
			<span class="icon-bar"></span></button>
			<a class="navbar-brand page-scroll" href="#page-top">PineApple</a>
		</div>
		<!-- Collect the nav links, forms, and other content for toggling -->
		<div id="bs-example-navbar-collapse-1" class="collapse navbar-collapse">
			<ul class="nav navbar-nav navbar-right">
				<li class="hidden"><a href="#page-top"></a></li>
				<li><a class="page-scroll" href="#services">Services</a> </li>
				<li><a class="page-scroll" href="#portfolio">Packages</a> </li>
				<li><a class="page-scroll" href="#about">Personal</a> </li>
				<li><a class="page-scroll" href="#team">Business</a> </li>
				<li><a class="page-scroll" href="contact.php">Contact</a> </li>
				<li class="blueResponsive">
				<a class="page-scroll" href="signin/profile.php"><?php echo $userName ?>
				</a></li>
			</ul>
		</div>
		<!-- /.navbar-collapse --></div>
	<!-- /.container-fluid -->
</nav>
<!-- Header -->
<header style="background: #3A79C0">
	<div class="container">
		<div class="intro-text">
			<div class="intro-lead-in">
				Welcome To PineApple!</div>
			<div class="intro-heading">
				It's Nice To Meet You</div>
			<a class="page-scroll btn btn-xl" href="#services">Get Started</a>
		</div>
	</div>
</header>
<!-- Services Section -->
<section id="services">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 text-center">
				<h2 class="section-heading">Services</h2>
				<h3 class="section-subheading text-muted"></h3>
			</div>
		</div>
		<div class="row text-center">
			<div class="col-md-4">
				<span class="fa-stack fa-4x">
				<i class="fa fa-circle fa-stack-2x text-primary"></i>
				<i class="fa fa-shopping-cart fa-stack-1x fa-inverse"></i>
				</span>
				<h4 class="service-heading">Phone</h4>
				<p class="text-muted"></p>
			</div>
			<div class="col-md-4">
				<span class="fa-stack fa-4x">
				<i class="fa fa-circle fa-stack-2x text-primary"></i>
				<i class="fa fa-laptop fa-stack-1x fa-inverse"></i></span>
				<h4 class="service-heading">Wireless</h4>
				<p class="text-muted"></p>
			</div>
			<div class="col-md-4">
				<span class="fa-stack fa-4x">
				<i class="fa fa-circle fa-stack-2x text-primary"></i>
				<i class="fa fa-lock fa-stack-1x fa-inverse"></i></span>
				<h4 class="service-heading">Internet</h4>
				<p class="text-muted"></p>
			</div>
		</div>
	</div>
</section>
<!-- Portfolio Grid Section -->
<section id="portfolio" style="background-color: #1BC179">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 text-center">
				<h2 class="section-heading">Packages</h2>
				<h3 class="section-subheading text-muted"></h3>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4 col-sm-6 portfolio-item">
				<a class="portfolio-link" data-toggle="modal" href="#portfolioModal1">
				<div class="portfolio-hover">
					<div class="portfolio-hover-content">
						<i class="fa fa-plus fa-3x"></i></div>
				</div>
				<img alt="" class="img-responsive" src="img/radio.png"> </a>
				<div class="portfolio-caption">
					<h4 class="btn btn-success h4Text">Wireless- Text, Voice, and Data</h4>
					<p class="text-muted">4G LTE</p>
				</div>
			</div>
			<div class="col-md-4 col-sm-6 portfolio-item">
				<a class="portfolio-link" data-toggle="modal" href="#portfolioModal2">
				<div class="portfolio-hover">
					<div class="portfolio-hover-content">
						<i class="fa fa-plus fa-3x"></i></div>
				</div>
				<img alt="" class="img-responsive" src="img/wireline.png"> </a>
				<div class="portfolio-caption">
					<h4 class="btn btn-warning h4Text">Wireline-Phone Service</h4>
					<p class="text-muted">Unlimited Local Calls</p>
				</div>
			</div>
			<div class="col-md-4 col-sm-6 portfolio-item">
				<a class="portfolio-link" data-toggle="modal" href="#portfolioModal3">
				<div class="portfolio-hover">
					<div class="portfolio-hover-content">
						<i class="fa fa-plus fa-3x"></i></div>
				</div>
				<img alt="" class="img-responsive" src="img/ipad.png"> </a>
				<div class="portfolio-caption">
					<h4 class="btn btn-info h4Text">Optic Fiber Internet</h4>
					<p class="text-muted">With High Speed Internet Connection
					</p>
				</div>
			</div>
			<div class="col-md-4 col-sm-6 portfolio-item">
				<a class="portfolio-link" data-toggle="modal" href="#portfolioModal4">
				<div class="portfolio-hover">
					<div class="portfolio-hover-content">
						<i class="fa fa-plus fa-3x"></i></div>
				</div>
				<img alt="" class="img-responsive" src="img/bundleA.png"> </a>
				<div class="portfolio-caption">
					<h4>WIRELINE + WIRELESS</h4>
				</div>
			</div>
			<div class="col-md-4 col-sm-6 portfolio-item">
				<a class="portfolio-link" data-toggle="modal" href="#portfolioModal5">
				<div class="portfolio-hover">
					<div class="portfolio-hover-content">
						<i class="fa fa-plus fa-3x"></i></div>
				</div>
				<img alt="" class="img-responsive" src="img/iphones.png"> </a>
				<div class="portfolio-caption">
					<h4>WIRELESS + INTERNET</h4>
				</div>
			</div>
			<div class="col-md-4 col-sm-6 portfolio-item">
				<a class="portfolio-link" data-toggle="modal" href="#portfolioModal6">
				<div class="portfolio-hover">
					<div class="portfolio-hover-content">
						<i class="fa fa-plus fa-3x"></i></div>
				</div>
				<img alt="" class="img-responsive" src="img/mobile.png"> </a>
				<div class="portfolio-caption">
					<h4>WIRELINE + INTERNET</h4>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- About Section -->
<section id="about">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 text-center">
				<h2 class="section-heading">About</h2>
				<h3 class="section-subheading text-muted"></h3>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<ul class="timeline">
					<li>
					<div class="timeline-image">
						<img alt="" class="img-circle img-responsive" src="img/about/1.jpg">
					</div>
					<div class="timeline-panel">
						<div class="timeline-heading">
							<h4>2009-2011</h4>
							<h4 class="subheading">Our Humble Beginnings</h4>
						</div>
						<div class="timeline-body">
							<p class="text-muted"></p>
						</div>
					</div>
					</li>
					<li class="timeline-inverted">
					<div class="timeline-image">
						<img alt="" class="img-circle img-responsive" src="img/about/2.jpg">
					</div>
					<div class="timeline-panel">
						<div class="timeline-heading">
							<h4>March 2011</h4>
							<h4 class="subheading">An Agency is Born</h4>
						</div>
						<div class="timeline-body">
							<p class="text-muted"></p>
						</div>
					</div>
					</li>
					<li class="timeline-inverted">
					<div class="timeline-image">
						<img alt="" class="img-circle img-responsive" src="img/about/4.jpg">
					</div>
					<div class="timeline-panel">
						<div class="timeline-heading">
							<h4>July 2014</h4>
							<h4 class="subheading">Phase Two Expansion</h4>
						</div>
						<div class="timeline-body">
							<p class="text-muted"></p>
						</div>
					</div>
					</li>
					<li class="timeline-inverted">
					<div class="timeline-image">
						<h4>Be Part <br>Of Our <br>Story!</h4>
					</div>
					</li>
				</ul>
			</div>
		</div>
	</div>
</section>
<!-- Team Section -->
<section id="team" class="bg-light-gray">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 text-center">
				<h2 class="section-heading">Our Amazing Team</h2>
				<h3 class="section-subheading text-muted"></h3>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-4">
				<div class="team-member">
					<img alt="" class="img-responsive img-circle" src="#">
					<h4></h4>
					<p class="text-muted">Lead Designer</p>
					<ul class="list-inline social-buttons">
						<li><a href="#"><i class="fa fa-twitter"></i></a></li>
						<li><a href="#"><i class="fa fa-facebook"></i></a></li>
						<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
					</ul>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="team-member">
					<img alt="" class="img-responsive img-circle" src="#">
					<h4></h4>
					<p class="text-muted">Lead Marketer</p>
					<ul class="list-inline social-buttons">
						<li><a href="#"><i class="fa fa-twitter"></i></a></li>
						<li><a href="#"><i class="fa fa-facebook"></i></a></li>
						<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
					</ul>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="team-member">
					<img alt="" class="img-responsive img-circle" src="#">
					<h4></h4>
					<p class="text-muted">Lead Developer</p>
					<ul class="list-inline social-buttons">
						<li><a href="#"><i class="fa fa-twitter"></i></a></li>
						<li><a href="#"><i class="fa fa-facebook"></i></a></li>
						<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
					</ul>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-8 col-lg-offset-2 text-center">
				<p class="large text-muted"></p>
			</div>
		</div>
	</div>
</section>
<div style="text-align: center">
	<ul class="nav nav-pills" style="display: inline-block">
		<li role="presentation"><a href="about.html">ABOUT</a></li>
		<li role="presentation"><a href="team.html">TEAM</a></li>
		<li role="presentation"><a href="contact.php">CONTACT</a></li>
	</ul>
</div>
<footer>
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<span class="copyright">Copyright &copy; PineApple 2014</span>
			</div>
			<div class="col-md-4">
				<ul class="list-inline quicklinks">
					<li><a href="#">Privacy Policy</a> </li>
					<li><a href="#">Terms of Use</a> </li>
				</ul>
			</div>
		</div>
	</div>
</footer>
<!-- Portfolio Modals -->
<!-- Use the modals below to showcase details about your portfolio projects! -->
<!-- Portfolio Modal 1 -->
<div id="portfolioModal1" aria-hidden="true" class="portfolio-modal modal fade" role="dialog" tabindex="-1">
	<div class="modal-content">
		<div class="close-modal" data-dismiss="modal">
			<div class="lr">
				<div class="rl">
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-lg-offset-2">
					<div class="modal-body">
						<!-- Project Details Go Here -->
						<h2>Wireless Services</h2>
						<p class="item-intro text-muted"></p>
						<img alt="" class="img-responsive" src="img/radio.png">
						<p>Unlimited texting, voice, and data</p>
						<p>Operates on the latest 4g LTE technology</p>
						<p>Texts, minutes, and data rollover to next month</p>
						<p>Text internationally at no extra charge</p>
						<p>$60 a month</p>
						<button class="btn btn-primary" data-dismiss="modal" type="button">
						<i class="fa fa-times"></i>Close Project</button>
                        
						<?php $userid=$userDetails['user_id'] ?>

						<button class="btn btn-primary" data-dismiss="modal" type="button">
						<i class="fa fa-times"></i>HOME</button>
						<!--added by Max --><?php $status="False"; $types="Wireline"?>
						<?php
			
              if(strpos($userDetails['last_name'],'Wireless')!== false){
                $status="Delete";
   
                }
              else{
                $status = "Add";
   
                }
             ?>
             <a href= "adda.php?serviceType=Wireless"class="btn btn-info"><?php echo $status ?> </a>
             
             
             
             
                        
                        
                        
                        </div>
                        
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Portfolio Modal 2 -->
<div id="portfolioModal2" aria-hidden="true" class="portfolio-modal modal fade" role="dialog" tabindex="-1">
	<div class="modal-content">
		<div class="close-modal" data-dismiss="modal">
			<div class="lr">
				<div class="rl">
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-lg-offset-2">
					<div class="modal-body">
						<h2>Phone Service</h2>
						<p class="item-intro text-muted"></p>
						<img alt="" class="img-responsive img-centered" src="img/wireline.png">
						<p>Includes unlimited local, regional toll and long distance 
						calling across the U.S. and to Canada and Puerto Rico.</p>
						<p>Additional Features Included: Voice Mail, Caller ID, 
						and Call Waiting, Call Forwarding and 3-Way Calling.</p>
						<p>For an additional charge international calls can be made.</p>
						<p>$40 a month</p>

						<?php $userid=$userDetails['user_id'] ?>

						<button class="btn btn-primary" data-dismiss="modal" type="button">
						<i class="fa fa-times"></i>HOME</button>
						<!--added by Max --><?php $status="False"; $types="Wireline"?>
						<?php
			
              if(strpos($userDetails['last_name'],'Wireline')!== false){
                $status="Delete";
   
                }
              else{
                $status = "Add";
   
                }
             ?>
             <a href= "adda.php?serviceType=Wireline"class="btn btn-info"><?php echo $status ?> </a>

             </div>


            						
                      
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Portfolio Modal 3 -->
<div id="portfolioModal3" aria-hidden="true" class="portfolio-modal modal fade" role="dialog" tabindex="-1">
	<div class="modal-content">
		<div class="close-modal" data-dismiss="modal">
			<div class="lr">
				<div class="rl">
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-lg-offset-2">
					<div class="modal-body">
						<!-- Project Details Go Here -->
						<h2>Optic Fiber Internet</h2>
						<p class="item-intro text-muted"></p>
						<img alt="" class="img-responsive img-centered" src="img/ipad.png">
						<p>Capable of speeds up to 1000 Mbps, cpmpared to Comcast's 
						150 Mbps offered at $115/month</p>
						<p>Wireless (802.11a/b/g/n/ac) technology</p>
						<p>Able to download a 5MB MP3 file in 0.005 seconds and 
						a 4GB movie in 4 seconds</p>
						<p>$50 a month</p>
						<button class="btn btn-primary" data-dismiss="modal" type="button">
						<i class="fa fa-times"></i>Close</button>
                        
						<?php $userid=$userDetails['user_id'] ?>

						<button class="btn btn-primary" data-dismiss="modal" type="button">
						<i class="fa fa-times"></i>HOME</button>
						<!--added by Max --><?php $status="False"; $types="Internet"?>
						<?php
			
              if(strpos($userDetails['last_name'],'Internet')!== false){
                $status="Delete";
   
                }
              else{
                $status = "Add";
   
                }
             ?>
             <a href= "adda.php?serviceType=Internet"class="btn btn-info"><?php echo $status ?> </a>
             
             
             
						</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Portfolio Modal 4 -->
<div id="portfolioModal4" aria-hidden="true" class="portfolio-modal modal fade" role="dialog" tabindex="-1">
	<div class="modal-content">
		<div class="close-modal" data-dismiss="modal">
			<div class="lr">
				<div class="rl">
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-lg-offset-2">
					<div class="modal-body">
						<!-- Project Details Go Here -->
						<h2>Wireless and wireline phone</h2>
						<p class="item-intro text-muted"></p>
						<img alt="" class="img-responsive img-centered" src="img/bundleA.png">
						<p>The wireless phone and wireline package</p>
						<p>Combines the best of wireless and wireline at a more affordable price</p>
						<p>$90 a month</p>
                        		<?php $userid=$userDetails['user_id'] ?>
						<button class="btn btn-primary" data-dismiss="modal" type="button">
						<i class="fa fa-times"></i>Close Project</button>
                        			<!--added by Max --><?php $status="False"; $types="WIRELINE_WIRELESS"?>
						<?php
			
              if(strpos($userDetails['last_name'],'WIRELINE_WIRELESS')!== false){
                $status="Delete";
   
                }
              else{
                $status = "Add";
   
                }
             ?>
             <a href= "adda.php?serviceType=WIRELINE_WIRELESS"class="btn btn-info"><?php echo $status ?> </a>
             
             
             
                        </div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Portfolio Modal 5 -->
<div id="portfolioModal5" aria-hidden="true" class="portfolio-modal modal fade" role="dialog" tabindex="-1">
	<div class="modal-content">
		<div class="close-modal" data-dismiss="modal">
			<div class="lr">
				<div class="rl">
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-lg-offset-2">
					<div class="modal-body">
						<!-- Project Details Go Here -->
						<h2>Wireless phone and internet</h2>
						<p class="item-intro text-muted"></p>
						<img alt="" class="img-responsive img-centered" src="img/iphones.png">
						<p>The wireless phone and internet package</p>
						<p>Combines the best of wireless and internet at a more affordable price</p>
						<p>$80 a month</p>
                        		<?php $userid=$userDetails['user_id'] ?>
						<button class="btn btn-primary" data-dismiss="modal" type="button">
						<i class="fa fa-times"></i>Close Project</button>
                                    			<!--added by Max --><?php $status="False"; $types="WIRELESS_INTERNET"?>
						<?php
			
              if(strpos($userDetails['last_name'],'WIRELESS_INTERNET')!== false){
                $status="Delete";
   
                }
              else{
                $status = "Add";
   
                }
             ?>
             <a href= "adda.php?serviceType=WIRELESS_INTERNET"class="btn btn-info"><?php echo $status ?> </a><strong></strong>
                        </div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Portfolio Modal 6 -->
<div id="portfolioModal6" aria-hidden="true" class="portfolio-modal modal fade" role="dialog" tabindex="-1">
	<div class="modal-content">
		<div class="close-modal" data-dismiss="modal">
			<div class="lr">
				<div class="rl">
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-lg-offset-2">
					<div class="modal-body">
						<!-- Project Details Go Here -->
						<h2>Wireline phone plus internet</h2>
						<p class="item-intro text-muted"></p>
						<img alt="" class="img-responsive img-centered" src="img/mobile.png">
						<p>The wirelinephone and internet package</p>
						<p>Combines the best of wireline and internet at a more affordable price</p>
						<p>$100 a month</p>
                        		<?php $userid=$userDetails['user_id'] ?>
						<button class="btn btn-primary" data-dismiss="modal" type="button">
						<i class="fa fa-times"></i>Close Project</button>
                                    			<!--added by Max --><?php $status="False"; $types="WIRELINE_INTERNET"?>
						<?php
			
              if(strpos($userDetails['last_name'],'WIRELINE_INTERNET')!== false){
                $status="Delete";
   
                }
              else{
                $status = "Add";
   
                }
             ?>
             <a href= "adda.php?serviceType=WIRELINE_INTERNET"class="btn btn-info"><?php echo $status ?> </a>
             
             
                        </div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
function myFunction() {
    alert("Signin First");
}
</script>
<!-- jQuery -->
<script src="js/jquery.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="js/bootstrap.min.js"></script>
<!-- Plugin JavaScript -->
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
<script src="js/classie.js"></script>
<script src="js/cbpAnimatedHeader.js"></script>
<!-- Contact Form JavaScript -->
<script src="js/jqBootstrapValidation.js"></script>
<script src="js/contact_me.js"></script>
<!-- Custom Theme JavaScript -->
<script src="js/agency.js"></script>
<script charset="utf-8" src="signin/assets/js/sha512.js" type="text/javascript"></script>
<script charset="utf-8" src="signin/ASLibrary/js/asengine.js" type="text/javascript"></script>
<script charset="utf-8" src="signin/ASLibrary/js/index.js" type="text/javascript"></script>
<script charset="utf-8" src="signin/ASLibrary/js/profile.js" type="text/javascript"></script>

</body>


</html>
